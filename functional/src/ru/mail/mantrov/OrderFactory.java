package ru.mail.mantrov;

public interface OrderFactory {

    default Order createOrderNonStarted(int items, int price) {
        return createOrder(OrderStatus.NOT_STARTED, items, price);
    }

    default Order createOrderProcessing(int items, int price) {
        return createOrder(OrderStatus.PROCESSING, items, price);
    }

    default Order createOrderCompleted(int items, int price) {
        return createOrder(OrderStatus.COMPLETED, items, price);
    }

    Order createOrder(OrderStatus status, int items, int price);
}
