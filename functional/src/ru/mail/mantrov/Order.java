package ru.mail.mantrov;

import java.text.DecimalFormat;

public class Order {
    private OrderStatus status;
    private final int items;
    private final double price;

    public Order(OrderStatus status, int items, double price) {
        this.status = status;
        this.items = items;
        this.price = price;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public int getItems() {
        return items;
    }

    public double getPrice() {
        return price;
    }

    public void changeStatus(OrderStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String formattedDouble = new DecimalFormat("#0.00").format(price);
        return "Order{" +
                "status=" + status +
                ", items=" + items +
                ", price=" + formattedDouble +
                '}';
    }
}
