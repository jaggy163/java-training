package ru.mail.mantrov;

public class Main {
    public static void main(String[] args) {
        OrderFactoryImpl factory = new OrderFactoryImpl();
        Order order = factory.createOrderNonStarted(10, 100);
        System.out.println(order);
    }
}
