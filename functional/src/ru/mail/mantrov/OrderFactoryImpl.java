package ru.mail.mantrov;

public class OrderFactoryImpl implements OrderFactory {
    @Override
    public Order createOrder(OrderStatus status, int items, int price) {
        return new Order(status, items, price);
    }
}
