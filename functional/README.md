# Java 8 and functional programming

## API and Optional

1. Modify your `Cache` from previous task and return `Optional#empty` instead of `null` if 
element is missing. 
1. We use `Optional` for return values, is it a good idea to use it as incoming parameters?
Write down your thoughts.

## Method references and Streams

1. Modify your `Order` class, create method which would return `boolean` depends on the value of
numeric field (which was added in previous task).
1. Modify your filter algorithm, use new method and `Streams API`.

## Default methods

1. Create new interface called `OrderFactory`. Interface should provide methods for 
creation of `Orders` with different `OrderStatuses`. For each status create `default` method.
What method should provide implementation of this interface? Write your own implementation.

## Comments

## API and Optional
1. Modified. `ru.mail.mantrov.copy.cache.Cache`. Если нам нужно избежать null'а на выходе с метода `get`, то использование
`Optional` может и будет оправдано. Но на мой взгляд, в данной ситуации изначальный вариант с null'ом был более читаемый
и возвращал сразу `V value` а не `Optional<V>`. На мой вкус, лучше разобраться с null'ом.
1. Я бы не стал использовать `Optional` ни как возвращаемое значение, ни как входной параметр. Если есть удобство от его
использования внутри класса, то пусть мы в конструкторе все входные значения обернем в `Optional`, а все возвращаемые 
значения распакуем. Но, наверное, использовать `Optional` как входной параметр еще хуже, потому что мы перекладываем
заботу о том, как упаковать возможный null на того, кто будет пользоваться нашим классом/методом.

## Method references and Streams
1. Done. `ru.mail.mantrov.copy.Main`.

## Default methods
1. Совсем не понял задания. Интерфес создал, методы для создания `Order` сделал. Для каждого возможного статуса свой
дефолтный метод. Имплементацию для этого интерфейса создал. Не понял "What method should provide implementation of this interface?".
