# Java Exceptions and Errors

## Getting started

1. Create your own exception and add it to signature of `io.humb1t.Main.LifeCycleAction.execute`. 
1. Create your own implementation of `java.lang.AutoCloseable` interface. Test it.
1. Create a new class. Implement constructor which should throw an exception during execution.
Write a simple program where you attempt to instantiate object of this class and assign it to a 
variable (declaration should be outside `try/catch` block). Catch an exception. Find out - what's
a value inside your variable. Write down your thoughts.

## Comments
1. I added `io.humb1t.Main.LifeCycleActionIllegalArgumentException`.
1. `ru.mail.mantrov.Recources` is the implementation of `java.lang.AutoCloseable` interface. I test it using class 
`ru.mail.mantrov.TestRecources`.
1. `ru.mail.mantrov.constructor.SimleClass` и `ru.mail.mantrov.constructor.Main`. I didn't really understand the task.
How can i instantiate object of this class outside `try/catch` block and catch an exception? In this case program will fall. 

So here's my guess:

I made two constructors in `SimpleClass`. In `main()` at First I declare the instance of SimpleClass using default constructor.
Then I attempt to instantiate object of this class using another constructor in `try/catch` block. Then catch an exception
and print `toString()` of the instance. As the result - constructor which throws an exception didn't apply. The reference
to object is still same after program catch an exception of new constructor.