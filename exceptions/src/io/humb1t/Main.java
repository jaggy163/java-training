package io.humb1t;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        try {
            new LifeCycleAction().execute(-2);
        } catch (LifeCycleActionExecutionException | LifeCycleActionIllegalArgumentException | AccessDeniedException e) {
            System.err.println(e.getLocalizedMessage());
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try (FileInputStream fileInputStream = new FileInputStream(args[0])) {

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class LifeCycleAction {
        public void execute(int arg) throws LifeCycleActionExecutionException, AccessDeniedException, LifeCycleActionIllegalArgumentException {
            if (arg<0) throw new LifeCycleActionIllegalArgumentException();
            throw new LifeCycleActionExecutionException();
        }
    }

    public static class LifeCycleActionExecutionException extends Exception {
    }

    public static class LifeCycleActionIllegalArgumentException extends IllegalArgumentException {
        @Override
        public String getLocalizedMessage() {
            return "";
        }
    }


    public void exceptionVsResult() {
        final String result1 = (String) this.returnResult().value;
        final String result2 = returnOptional().orElse("");
        String result3 = "";
        try {
            result3 = returnValueOrThrowException();
        } catch (AccessDeniedException e) {
        }
    }

    private Result returnResult() {
        return Result.OK.setValue("OK");
    }

    private Optional<String> returnOptional() {
        return Optional.of("OK");
    }

    private String returnValueOrThrowException() throws AccessDeniedException {
        return "OK";
    }
}
