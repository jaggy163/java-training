package ru.mail.mantrov.constructor;

public class SimpleClass {
    public int numberVar;
    public String stringVar;

    SimpleClass() {
        numberVar = -1;
        stringVar = "!!!";
    }

    SimpleClass(int numberVar, String stringVar) {
        this.numberVar = numberVar;
        this.stringVar = stringVar;
        throw new RuntimeException();
    }

    @Override
    public String toString() {
        return "SimpleClass{" +
                "numberVar=" + numberVar +
                ", stringVar='" + stringVar + '\'' +
                '}';
    }
}
