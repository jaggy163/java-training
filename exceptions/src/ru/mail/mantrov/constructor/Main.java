package ru.mail.mantrov.constructor;

public class Main {
    public static void main(String[] args) {
        SimpleClass simpleClass = new SimpleClass();
        try {
            simpleClass = new SimpleClass(5, "Hey");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(simpleClass);
    }
}
