package ru.mail.mantrov;

// Print any number into the console to successful completion of the program. Print anything else to catch exception.
public class TestResources {
    public static void main(String[] args) {
        try(Resources res = new Resources()) {
            res.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
