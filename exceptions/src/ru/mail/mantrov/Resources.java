package ru.mail.mantrov;

import java.io.IOException;
import java.util.Scanner;

public class Resources implements AutoCloseable {

    public void connect() {
        System.out.println("Method 'connect()' called.");
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Method 'close()' called.");
    }
}
