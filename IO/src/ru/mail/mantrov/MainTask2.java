package ru.mail.mantrov;

import java.io.*;

public class MainTask2 {
    public static void main(String[] args) {
        File file = new File("IO/src/ru/mail/mantrov/text.txt");
        File fileResult = new File("IO/src/ru/mail/mantrov/text2.txt");
        String result = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file)) ; BufferedWriter bw = new BufferedWriter(new FileWriter(fileResult))) {
            String str = "";
            while ((str = br.readLine()) != null) {
                bw.write(new StringBuilder(str).reverse().append(System.lineSeparator()).toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}