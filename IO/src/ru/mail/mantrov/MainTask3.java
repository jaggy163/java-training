package ru.mail.mantrov;

import java.io.*;

public class MainTask3 {
    public static void main(String[] args) {
        Product product = new Product("Пончик", 20.0f, 22);
        File file = new File("IO/src/ru/mail/mantrov/product.txt");
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file)) ;
             DataOutputStream dos = new DataOutputStream(new FileOutputStream(file))) {
            dos.writeUTF(product.getName());
            dos.writeFloat(product.getPrice());
            dos.writeInt(product.getCount());

            String productName = dis.readUTF();
            float productPrice = dis.readFloat();
            int productCount = dis.readInt();
            System.out.println(productName + " " + productPrice + " " + productCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
