package ru.mail.mantrov;

import java.io.*;
import java.util.Scanner;

public class MainTask5 {
    public static void main(String[] args) {
        System.out.println("Впишите имя файла, с которого будет считана информация и выведена на консоль:");
        read();
    }

    private static void read() {
        Scanner scanner = new Scanner(System.in);
        String filePath = scanner.nextLine();
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            System.out.println("Файл открыт. Для вывода каждой строки вводите любой символ и нажимайте Enter :");
            String line;
            while ((line = br.readLine()) != null) {
                scanner.next();
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Файл не найден. Попробуйте еще раз:");
            read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
