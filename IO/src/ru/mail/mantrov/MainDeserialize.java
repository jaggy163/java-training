package ru.mail.mantrov;

import java.io.*;

public class MainDeserialize {
    public static void main(String[] args) {
        File file = new File("IO/src/ru/mail/mantrov/prod-obj");
        Product product = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            product = (Product) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(product);
    }
}
