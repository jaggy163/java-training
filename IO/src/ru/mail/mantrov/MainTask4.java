package ru.mail.mantrov;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MainTask4 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Task4Class class1 = new Task4Class(list);
        Task4Class class2 = new Task4Class(list);
        Task4Class class1Copy = null;
        Task4Class class2Copy = null;

        list.add(5);
        list.add(17);

        File file1 = new File("IO/src/ru/mail/mantrov/obj1");
        File file2 = new File("IO/src/ru/mail/mantrov/obj2");
        try(ObjectOutputStream oos1 = new ObjectOutputStream(new FileOutputStream(file1)) ;
            ObjectOutputStream oos2 = new ObjectOutputStream(new FileOutputStream(file2)) ;
            ObjectInputStream ois1 = new ObjectInputStream(new FileInputStream(file1)) ;
            ObjectInputStream ois2 = new ObjectInputStream(new FileInputStream(file2))) {

            oos1.writeObject(class1);
            oos2.writeObject(class2);

            class1Copy = (Task4Class) ois1.readObject();
            class2Copy = (Task4Class) ois2.readObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        System.out.println("Class 1 : " + class1Copy + "\nClass 2 : " + class2Copy);
        list.add(7);
        class1Copy.getList().add(9);
        System.out.println("\nClass 1 : " + class1Copy + "\nClass 2 : " + class2Copy);
    }
}
