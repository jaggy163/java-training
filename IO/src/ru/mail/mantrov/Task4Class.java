package ru.mail.mantrov;

import java.io.Serializable;
import java.util.List;

public class Task4Class implements Serializable {
    private final List<Integer> list;

    public Task4Class(List list) {
        this.list = list;
    }

    public List<Integer> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Task4Class{" +
                "list=" + list +
                '}';
    }
}
