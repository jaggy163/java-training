package ru.mail.mantrov;

import java.io.*;

public class MainSerialize {
    public static void main(String[] args) {
        Product product = new Product("Конфета", 12.0f, 167);
        File file = new File("IO/src/ru/mail/mantrov/prod-obj");
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(product);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
