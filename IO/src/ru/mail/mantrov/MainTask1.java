package ru.mail.mantrov;

import java.io.*;

public class MainTask1 {
    public static void main(String[] args) {
        File file = new File("IO/src/ru/mail/mantrov/text.txt");
        String result = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String str = "";
            while ((str = br.readLine()) != null) {
                result = result.concat(str);
            }
            result = new StringBuilder(result).reverse().toString();
            System.out.println(result);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
