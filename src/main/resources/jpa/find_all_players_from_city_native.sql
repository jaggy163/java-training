SELECT player_from_city.*, player_info.birth_date FROM
 (SELECT players.*, club_city.title, club_city.name FROM
 players INNER JOIN
 (SELECT clubs.id, clubs.title, cities.name FROM clubs INNER JOIN cities on clubs.city_id=cities.id WHERE cities.name='Moscow') AS club_city
 ON players.club_id=club_city.id) AS player_from_city
 INNER JOIN
 player_info ON player_from_city.id = player_info.id
 Order by player_info.birth_date