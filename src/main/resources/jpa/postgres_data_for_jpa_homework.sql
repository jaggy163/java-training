-- �������� � ���������� ������
DROP TABLE IF EXISTS cities, clubs, players, player_info, competitions, club_competition;

CREATE TABLE IF NOT EXISTS cities (
    id serial PRIMARY KEY,
    name VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS clubs (
    id serial PRIMARY KEY,
    city_id INT references cities(id) NOT NULL,
    title VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS players (
    id serial PRIMARY KEY,
    club_id INT references clubs(id) NOT NULL,
    first_name VARCHAR(25) NOT NULL,
    second_name VARCHAR(25) NOT NULL
);

CREATE TABLE IF NOT EXISTS player_info (
    id serial PRIMARY KEY references players(id),
    birth_date DATE NOT NULL,
    number INT
);

CREATE TABLE IF NOT EXISTS competitions (
    id serial PRIMARY KEY,
    competition_name VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS club_competition (
    id_club INT PRIMARY KEY references clubs(id),
    id_competition INT PRIMARY KEY references competitions(id)
);

INSERT INTO cities(name) VALUES('Moscow');
INSERT INTO cities(name) VALUES('Saint-Petersburg');
INSERT INTO cities(name) VALUES('Krasnodar');
INSERT INTO cities(name) VALUES('Rostov-on-the-Don');
INSERT INTO cities(name) VALUES('Samara');
INSERT INTO cities(name) VALUES('Yekaterinburg');
INSERT INTO cities(name) VALUES('Kazan');
INSERT INTO cities(name) VALUES('Ufa');
INSERT INTO cities(name) VALUES('Orenburg');
INSERT INTO cities(name) VALUES('Sochi');
INSERT INTO cities(name) VALUES('Grozny');
INSERT INTO cities(name) VALUES('Tula');
INSERT INTO cities(name) VALUES('Tambov');

INSERT INTO clubs(city_id, title) VALUES(1, 'Dinamo');
INSERT INTO clubs(city_id, title) VALUES(1, 'CSKA');
INSERT INTO clubs(city_id, title) VALUES(1, 'Spartak');
INSERT INTO clubs(city_id, title) VALUES(1, 'Lokomotiv');
INSERT INTO clubs(city_id, title) VALUES(2, 'Zenit');
INSERT INTO clubs(city_id, title) VALUES(3, 'Krasnodar');
INSERT INTO clubs(city_id, title) VALUES(4, 'Rostov');
INSERT INTO clubs(city_id, title) VALUES(5, 'Krylya Sovetov');
INSERT INTO clubs(city_id, title) VALUES(6, 'Ural');
INSERT INTO clubs(city_id, title) VALUES(7, 'Rubin');
INSERT INTO clubs(city_id, title) VALUES(8, 'Ufa');
INSERT INTO clubs(city_id, title) VALUES(9, 'Orenburg');
INSERT INTO clubs(city_id, title) VALUES(10, 'Sochi');
INSERT INTO clubs(city_id, title) VALUES(11, 'Akhmat');
INSERT INTO clubs(city_id, title) VALUES(12, 'Arsenal');
INSERT INTO clubs(city_id, title) VALUES(13, 'Tambov');

INSERT INTO players(club_id, first_name, second_name)
    VALUES(1, 'Grigory', 'Morozov');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1994-06-06', 2);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(1, 'Vyacheslav', 'Grulev');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1999-03-23', 20);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(2, 'Mario', 'Fernandes');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1990-09-19', 2);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(2, 'Fedor', 'Chalov');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1998-04-10', 9);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(3, 'Roman', 'Zobnin');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1994-02-11', 47);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(4, 'Fedor', 'Smolov');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1990-02-09', 9);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(4, 'Grzegorz', 'Krychowiak');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1990-01-29', 7);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(5, 'Yury', 'Zhirkov');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1983-08-20', 18);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(5, 'Andrey', 'Lunev');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1991-11-13', 99);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(5, 'Artem', 'Dzyuba');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1988-08-22', 22);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(7, 'Roman', 'Eremenko');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1987-03-19', 7);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(8, 'Anton', 'Zinkovsky');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1996-04-14', 17);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(8, 'Egor', 'Golenkov');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1999-07-07', 69);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(8, 'Alexander', 'Sobolev');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1997-03-07  ', 7);

INSERT INTO players(club_id, first_name, second_name)
    VALUES(8, 'Taras', 'Burlak');
INSERT INTO player_info(birth_date, number)
    VALUES(DATE '1990-02-22', 90);

INSERT INTO competitions(competition_name) VALUES('RPL');
INSERT INTO competitions(competition_name) VALUES('Russian Cup');
INSERT INTO competitions(competition_name) VALUES('Europa League');
INSERT INTO competitions(competition_name) VALUES('Champions League');

INSERT INTO club_competition(id_club, id_competition) VALUES(1, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(2, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(2, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(2, 3);
INSERT INTO club_competition(id_club, id_competition) VALUES(3, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(3, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(4, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(4, 4);
INSERT INTO club_competition(id_club, id_competition) VALUES(5, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(5, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(5, 4);
INSERT INTO club_competition(id_club, id_competition) VALUES(6, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(6, 3);
INSERT INTO club_competition(id_club, id_competition) VALUES(7, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(7, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(8, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(9, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(9, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(10, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(11, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(11, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(12, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(12, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(13, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(14, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(14, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(15, 1);
INSERT INTO club_competition(id_club, id_competition) VALUES(15, 2);
INSERT INTO club_competition(id_club, id_competition) VALUES(16, 1);