CREATE TRIGGER cpfk
 BEFORE DELETE ON players FOR EACH ROW
 EXECUTE PROCEDURE public.check_player_from_kc();