CREATE OR REPLACE FUNCTION public.check_player_from_kc() RETURNS TRIGGER AS
 $cpfk$
 BEGIN
    IF OLD.club_id = 8 THEN
        RAISE EXCEPTION 'KS player cannot be deleted';
    END IF;
    RETURN OLD;
 END;
 $cpfk$ LANGUAGE plpgsql;