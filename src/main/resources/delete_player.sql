CREATE OR REPLACE PROCEDURE public.deletePlayer(IN player_id integer)
LANGUAGE 'sql'
AS $$
    DELETE FROM players WHERE players.id = player_id;
$$;