-- �������� � ���������� ������
DROP TABLE IF EXISTS cities, clubs, players;

CREATE TABLE IF NOT EXISTS cities (
    id serial PRIMARY KEY,
    name VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS clubs (
    id serial PRIMARY KEY,
    city_id INT references cities(id) NOT NULL,
    title VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS players (
    id serial PRIMARY KEY,
    club_id INT references clubs(id) NOT NULL,
    first_name VARCHAR(25) NOT NULL,
    second_name VARCHAR(25) NOT NULL,
    birth_date DATE NOT NULL,
    number INT
);

INSERT INTO cities(name) VALUES('Moscow');
INSERT INTO cities(name) VALUES('Saint-Petersburg');
INSERT INTO cities(name) VALUES('Krasnodar');
INSERT INTO cities(name) VALUES('Rostov-on-the-Don');
INSERT INTO cities(name) VALUES('Samara');
INSERT INTO cities(name) VALUES('Yekaterinburg');
INSERT INTO cities(name) VALUES('Kazan');
INSERT INTO cities(name) VALUES('Ufa');
INSERT INTO cities(name) VALUES('Orenburg');
INSERT INTO cities(name) VALUES('Sochi');
INSERT INTO cities(name) VALUES('Grozny');
INSERT INTO cities(name) VALUES('Tula');
INSERT INTO cities(name) VALUES('Tambov');

INSERT INTO clubs(city_id, title) VALUES(1, 'Dinamo');
INSERT INTO clubs(city_id, title) VALUES(1, 'CSKA');
INSERT INTO clubs(city_id, title) VALUES(1, 'Spartak');
INSERT INTO clubs(city_id, title) VALUES(1, 'Lokomotiv');
INSERT INTO clubs(city_id, title) VALUES(2, 'Zenit');
INSERT INTO clubs(city_id, title) VALUES(3, 'Krasnodar');
INSERT INTO clubs(city_id, title) VALUES(4, 'Rostov');
INSERT INTO clubs(city_id, title) VALUES(5, 'Krylya Sovetov');
INSERT INTO clubs(city_id, title) VALUES(6, 'Ural');
INSERT INTO clubs(city_id, title) VALUES(7, 'Rubin');
INSERT INTO clubs(city_id, title) VALUES(8, 'Ufa');
INSERT INTO clubs(city_id, title) VALUES(9, 'Orenburg');
INSERT INTO clubs(city_id, title) VALUES(10, 'Sochi');
INSERT INTO clubs(city_id, title) VALUES(11, 'Akhmat');
INSERT INTO clubs(city_id, title) VALUES(12, 'Arsenal');
INSERT INTO clubs(city_id, title) VALUES(13, 'Tambov');

INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(1, 'Grigory', 'Morozov', DATE '1994-06-06', 2);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(1, 'Vyacheslav', 'Grulev', DATE '1999-03-23', 20);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(2, 'Mario', 'Fernandes', DATE '1990-09-19', 2);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(2, 'Fedor', 'Chalov', DATE '1998-04-10', 9);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(3, 'Roman', 'Zobnin', DATE '1994-02-11', 47);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(4, 'Fedor', 'Smolov', DATE '1990-02-09', 9);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(4, 'Grzegorz', 'Krychowiak', DATE '1990-01-29', 7);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(5, 'Yury', 'Zhirkov', DATE '1983-08-20', 18);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(5, 'Andrey', 'Lunev', DATE '1991-11-13', 99);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(5, 'Artem', 'Dzyuba', DATE '1988-08-22', 22);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(7, 'Roman', 'Eremenko', DATE '1987-03-19', 7);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(8, 'Anton', 'Zinkovsky', DATE '1996-04-14', 17);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(8, 'Egor', 'Golenkov', DATE '1999-07-07', 69);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(8, 'Alexander', 'Sobolev', DATE '1997-03-07  ', 7);
INSERT INTO players(club_id, first_name, second_name, birth_date, number)
    VALUES(8, 'Taras', 'Burlak', DATE '1990-02-22', 90);