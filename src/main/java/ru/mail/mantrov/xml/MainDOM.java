package ru.mail.mantrov.xml;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.events.Attribute;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class MainDOM {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        File file = new File("src/main/java/ru/mail/mantrov/xml/retrieveCurrentCPFFUNCP.xml");

        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document dom = builder.parse(file);

        dom.getDocumentElement().normalize();

        if (!findStatusM(dom)) {
            System.out.println("Ошибка!");
        }
    }

    public static boolean findStatusM(Document dom) {
        NodeList nodeList = dom.getElementsByTagName("ns14:Obs");
        for (int i=0; i<nodeList.getLength(); i++) {
            NamedNodeMap map = nodeList.item(i).getAttributes();
            Node node = map.getNamedItem("OBS_STATUS");
            if (node.getNodeValue().equals("M")) {
                System.out.println("Нашел " + nodeList.item(i).getNodeName() + "\nВот его текстовое поле: "
                        + nodeList.item(i).getTextContent());
                return true;
            }
        }
        return false;
    }
}
