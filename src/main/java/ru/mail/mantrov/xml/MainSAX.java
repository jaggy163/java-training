package ru.mail.mantrov.xml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;

public class MainSAX {
    public static void main(String[] args) throws MalformedURLException {

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        File file = new File("src/main/java/ru/mail/mantrov/xml/retrieveCurrentCPFFUNCP.xml");

        try {
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
            if (!findStatusM(reader)) {
                System.out.println("Ошибка!");
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static boolean findStatusM(XMLEventReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            XMLEvent xmlEvent = reader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                Iterator<Attribute> attributes = startElement.getAttributes();
                if (checkAttribute(attributes)) {
                    System.out.println("Найден элемент " + startElement + " с аттрибутом OBS_STATUS равным \"M\"");
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean checkAttribute(Iterator<Attribute> attributes) {
        while (attributes.hasNext()) {
            Attribute attribute = attributes.next();
            if (attribute.getName().toString().equals("OBS_STATUS")) {
                String value = attribute.getValue();
                if (value.equals("M")) {
                    return true;
                }
            }
        }
        return false;
    }
}
