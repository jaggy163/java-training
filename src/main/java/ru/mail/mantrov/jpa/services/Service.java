package ru.mail.mantrov.jpa.services;

import ru.mail.mantrov.jpa.dao.Dao;
import ru.mail.mantrov.jpa.pojo.Club;
import ru.mail.mantrov.jpa.pojo.Player;

import javax.persistence.Tuple;
import java.util.List;


public class Service {
    private Dao dao = new Dao();

    /*
    Returns all players from the table
     */
    public List<Player> findAllPlayers() {
        return dao.findAllPlayers();
    }

    /*
    Returns all players from the city - 'cityName'. Realized throw the JPQL
     */
    public List<Player> findAllPlayersFromCityJPQL(String cityName) {
        return dao.findAllPlayersFromCityJPQL(cityName);
    }

    /*
    Returns all players from the city - 'cityName'. Realized throw the NamedQuery
     */
    public List<Player> findAllPlayersFromCityNamedQuery(String cityName) {
        return dao.findAllPlayersFromCityNamedQuery(cityName);
    }

    /*
    Returns all players from the city - 'Moscow'. Realized throw the NativeQuery
     */
    public List<String> findAllPlayersFromMoscowNativeQuery() {
        return dao.findAllPlayersFromMoscowNativeQuery();
    }

    /*
    Returns all players from the city - 'cityName'. Realized throw the Criteria API
     */
    public List<Player> findAllPlayersFromCityCriteria(String cityName) {
        return dao.findAllPlayersFromCityCriteria(cityName);
    }

    /*
    Returns all clubs which have 'minPlayerCount' players or more. Realized throw the NamedQuery
     */
    public List<Club> findAllClubsWithPlayers(int minPlayerCount) {
        return dao.findAllClubsWithPlayers(minPlayerCount);
    }

    /*
    Returns all clubs which have 'minPlayerCount' players or more. Realized throw the JPQL
     */
    public List<Club> findAllClubsWithPlayersJPQL(int minPlayerCount) {
        return dao.findAllClubsWithPlayersJPQL(minPlayerCount);
    }

    /*
    Returns all clubs which have '1' players or more. Realized throw the NativeQuery
     */
    public List<String> findAllClubsWithPlayersNativeQuery() {
        return dao.findAllClubsWithPlayersNativeQuery();
    }

    /*
    Returns all clubs which have 'minPlayerCount' players or more. Realized throw the Criteria API
     */
    public List<Tuple> findAllClubsWithPlayersCriteria(long minPlayerCount) {
        return dao.findAllClubsWithPlayersCriteria(minPlayerCount);
    }
}
