package ru.mail.mantrov.jpa.dao;


import ru.mail.mantrov.jpa.pojo.Club;
import ru.mail.mantrov.jpa.pojo.Player;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.util.List;


public class Dao {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("Mantrov");
    private EntityManager entityManager = emf.createEntityManager();
    private final static String FIND_ALL_PLAYERS_FROM_CITY_JPQL_QUERY = "SELECT p FROM\n" +
            " Player p JOIN p.club cl\n" +
            " WHERE cl.city.name = :name\n" +
            " ORDER BY p.playerInfo.birthDate";
    private final static String FIND_ALL_CLUBS_WITH_PLAYERS_JPQL = "SELECT c\n" +
            " FROM Club c\n" +
            " JOIN c.players p\n" +
            " GROUP BY c\n" +
            " HAVING COUNT(p) >= :minPlayerCount";


    public List<Player> findAllPlayers() {
        List<Player> players = (List<Player>) entityManager.createQuery("From Player").getResultList();
        return players;
    }

    public List<Player> findAllPlayersFromCityJPQL(String cityName) {
        List<Player> players = (List<Player>) entityManager.createQuery(FIND_ALL_PLAYERS_FROM_CITY_JPQL_QUERY)
                .setParameter("name", cityName).getResultList();
        return players;
    }

    public List<Player> findAllPlayersFromCityNamedQuery(String cityName) {
        return entityManager.createNamedQuery(Player.FIND_ALL_PLAYERS_FROM_CITY)
                .setParameter("name", cityName).getResultList();
    }

    public List<String> findAllPlayersFromMoscowNativeQuery() {
        return entityManager.createNativeQuery("SELECT player_from_city.*, player_info.birth_date FROM\n" +
                " (SELECT players.*, club_city.title, club_city.name FROM\n" +
                " players INNER JOIN\n" +
                " (SELECT clubs.id, clubs.title, cities.name FROM clubs INNER JOIN cities on clubs.city_id=cities.id WHERE cities.name='Moscow') AS club_city\n" +
                " ON players.club_id=club_city.id) AS player_from_city\n" +
                " INNER JOIN\n" +
                " player_info ON player_from_city.id = player_info.id\n" +
                " Order by player_info.birth_date").getResultList();
    }

    public List<Player> findAllPlayersFromCityCriteria(String cityName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Player> criteriaQuery = criteriaBuilder.createQuery(Player.class);
        Root<Player> player = criteriaQuery.from(Player.class);
        Join<Player, Club> joinPlayerClub = player.join("club");
        joinPlayerClub.join("city");
        criteriaQuery.select(player).where(criteriaBuilder.equal((player.get("club").get("city").get("name")), cityName));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public List<Club> findAllClubsWithPlayersJPQL(long minPlayerCount) {
        List<Club> clubs = (List<Club>) entityManager.createQuery(FIND_ALL_CLUBS_WITH_PLAYERS_JPQL)
                .setParameter("minPlayerCount", minPlayerCount).getResultList();
        return clubs;
    }

    public List<Club> findAllClubsWithPlayers(long minPlayerCount) {
        List<Club> clubs = (List<Club>) entityManager.createNamedQuery(Club.FIND_ALL_CLUBS_WITH_PLAYERS)
                .setParameter("minPlayerCount", minPlayerCount).getResultList();
        return clubs;
    }

    public List<String> findAllClubsWithPlayersNativeQuery() {
        return entityManager.createNativeQuery("SELECT clubs.* FROM clubs INNER JOIN players ON clubs.id = players.club_id" +
                " GROUP BY clubs.id HAVING COUNT(players) >= 3").getResultList();
    }

    public List<Tuple> findAllClubsWithPlayersCriteria(long minPlayerCount) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createQuery(Tuple.class);
        Root<Club> club = criteriaQuery.from(Club.class);
        Join<Club, Player> joinClubPlayer = club.join("players");
        criteriaQuery.multiselect(club.get("title"), criteriaBuilder.count(joinClubPlayer.get("club")))
                .groupBy(club).having(criteriaBuilder.ge(criteriaBuilder.count(joinClubPlayer.get("club")), minPlayerCount));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
