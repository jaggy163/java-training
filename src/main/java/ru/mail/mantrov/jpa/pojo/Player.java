package ru.mail.mantrov.jpa.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(name="players")
@NamedQueries({
        @NamedQuery(name = Player.FIND_ALL_PLAYERS_FROM_CITY, query = "SELECT p FROM\n" +
                " Player p JOIN p.club cl\n" +
                " WHERE cl.city.name = :name\n" +
                " ORDER BY p.playerInfo.birthDate")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "nativeSQL", query = "SELECT second_name FROM players")
})
public class Player {
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(targetEntity = Club.class)
    @JoinColumn(name="club_id")
    private Club club;

    @Column(name="first_name")
    private String firstName;

    @Column(name="second_name")
    private String secondName;

    @OneToOne
    @PrimaryKeyJoinColumn
    private PlayerInfo playerInfo;

    public static final String  FIND_ALL_PLAYERS_FROM_CITY = "findAllPlayersFromCity";
    public static final String FIND_ALL_PLAYERS_NATIVE = "findAllFromMoscow";

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }
}
