package ru.mail.mantrov.jpa.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@ToString
@Entity
@Table(name="player_info")
public class PlayerInfo {
    @Id
    private long id;

    @ToString.Exclude
    @OneToOne(mappedBy = "playerInfo")
    private Player player;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "number")
    private int number;
}
