package ru.mail.mantrov.database;

import org.apache.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class DBToolsForWork {
    Logger logger = Logger.getLogger(DBToolsForWork.class);
    public Connection connection;

    public DBToolsForWork(Connection connection) {
        this.connection = connection;
    }

    public void executeSQLScript(String url) {
        File file = new File(url);
        StringBuilder queries = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String str;
            while ((str = br.readLine()) != null) {
                queries.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Pattern p = Pattern.compile(";");
        Scanner scanner = new Scanner(queries.toString()).useDelimiter(p);
        try(Statement st = connection.createStatement()) {
            while (scanner.hasNext()) {
                String s = scanner.next() + ";";
                System.out.println(s);
                st.executeUpdate(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void executeSQLScriptOverall(String url) {
        File file = new File(url);
        StringBuilder queries = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            String str;
            while ((str = br.readLine()) != null) {
                queries.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try(Statement st = connection.createStatement()) {
            st.execute(queries.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String query) throws SQLException {
        Statement st = connection.createStatement();
        return st.executeQuery(query);
    }

    public ResultSet getClubPlayers(String club) throws SQLException {
        String request = "SELECT first_name, second_name, number FROM(" +
                "players INNER JOIN clubs ON players.club_id=clubs.id)" +
                "WHERE clubs.title='" + club + "'";
        PreparedStatement preparedStatement = connection.prepareStatement(request);
        return preparedStatement.executeQuery();
    }

    public ResultSet deletePlayer(int player_id) throws  SQLException {
        String request = "CALL deletePlayer(?)";
        CallableStatement cstmt = connection.prepareCall(request);
        cstmt.setInt(1, player_id);
        cstmt.execute();
        return cstmt.getResultSet();
    }

    public void transferChange(int playerID1, int playerID2) throws SQLException {
        String request = "BEGIN; " +
                "CREATE TEMP TABLE clubID " +
                "ON COMMIT DROP " +
                "AS " +
                "SELECT 1 AS id; " +
                "UPDATE clubID " +
                "SET id = (SELECT club_id FROM players WHERE id = " + playerID1 + "); " +
                "UPDATE players " +
                "SET club_id = (SELECT club_id FROM players WHERE id = " + playerID2 + ") " +
                "WHERE id = " + playerID1 + "; " +
                "UPDATE players " +
                "SET club_id = (SELECT id FROM clubID) " +
                "WHERE id = " + playerID2 + "; " +
                "COMMIT;";
        Statement st = connection.createStatement();
        st.execute(request);
        logger.debug("Трансфер выполнен. Обмен игроками " + playerID1 + " и "+ playerID2 + " закончился успешно.");
    }
}
