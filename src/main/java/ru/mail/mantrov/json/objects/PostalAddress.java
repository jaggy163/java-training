package ru.mail.mantrov.json.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
@Setter
@Getter
public class PostalAddress {
	private List<String> countrySubDivision;
	private List<String> addressLine;
	private String townName;
	private String country;
	private GeoLocation geoLocation;
	private String postCode;
}
