package ru.mail.mantrov.json.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@EqualsAndHashCode
@Setter
@Getter
public class JsonContainer {
	private List<DataEntry> data;
}
