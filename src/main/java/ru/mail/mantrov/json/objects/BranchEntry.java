package ru.mail.mantrov.json.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.util.List;
import java.util.Map;

@ToString
@EqualsAndHashCode
@Getter
@Setter
public class BranchEntry {
	private Availability availability;
	private PostalAddress postalAddress;
	private String type;
	private List<String> customerSegment;
	private List<String> accessibility;
	private Integer sequenceNumber;
	private Integer identification;
	private List<Map<String, String>> contactInfo;
	private String name;
	private List<Map<String, String>> otherServiceAndFacility;
}
