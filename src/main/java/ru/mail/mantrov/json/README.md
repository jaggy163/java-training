## Comments
1. `java.ru.mail.mantrov.InformationReaderMain` - �������� ����� ru.mail.mantrov.json. ��������� �� ���� "Brand", ������� ������ � 
���� ��� `Value`. �������� ��� ����� `value` � ������ ��� � �������.
1. � ������� [JsonToJava](https://github.com/astav/JsonToJava) ������ ������ ��� ��������������. ������� � ���
�������� `lombok.ToString` � `lombok.EqualsAndHashCode`, ����� ������� ���������� ������� � ������� � ����� �������� ��
����� `#equals`.
1. ��������� Jackson ������ �������������� ������� �� ����� example.json. ���������� ������ ������� � result.json.
�����, ����� ���������, ��� �������������� � ������������, ���������� ����, �������� ��������� - ������ ������ ��
result.json � ������� ��� � ���, ��� ��� ������ �� example.json. ��� ��������� ���������.