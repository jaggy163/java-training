package ru.mail.mantrov.json;



import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.mail.mantrov.json.objects.JsonContainer;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        File in = new File("src/main/java/ru/mail/mantrov/json/example.json");
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES);
        mapper.enable(MapperFeature.USE_STD_BEAN_NAMING);
        JsonContainer jsonDataIn = mapper.readValue(in, JsonContainer.class);

        File out = new File("src/main/java/ru/mail/mantrov/json/result.json");
        mapper.enable(SerializationFeature.INDENT_OUTPUT).writeValue(out, jsonDataIn);
        JsonContainer jsonDataOut = mapper.readValue(out, JsonContainer.class);

        System.out.println(jsonDataIn + "\n" + jsonDataOut + "\n" + jsonDataIn.equals(jsonDataOut));
    }
}
