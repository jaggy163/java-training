package testing.ru.mail.mantrov;

public class NumberRowSupplier {
    public static int[] staticRow() {
        int[] out = {1,2,3};
        return out;
    }

    public final int[] finalRow() {
        int[] out = {4,5,6};
        return out;
    }

    private int[] privateRow() {
        int[] out = {7,8,9};
        return out;
    }

    public int[] getPrivateRow() {
        return privateRow();
    }
}
