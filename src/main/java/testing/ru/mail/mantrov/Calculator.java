package testing.ru.mail.mantrov;

import org.apache.log4j.Logger;

public class Calculator {
    Logger logger = Logger.getLogger(Calculator.class);

    public Calculator() {
        logger.info("Создан объект класса " + Calculator.class.getName());
    }

    public int addition(int x, int y) {
        logger.debug("Вход метода addition. Аргументы: " + x + ", " + y);
        int r = x + y;
        if (((x ^ r) & (y ^ r)) < 0) {
            logger.error("Ошибка в методе addition.");
            throw new ArithmeticException("integer overflow");
        }
        logger.debug("Успешный выход из метода addition. Возвращаемый результат: " + r);
        return r;
    }

    public int subtraction(int x, int y) {
        logger.debug("Вход метода subtraction. Аргументы: " + x + ", " + y);
        int r = x - y;
        if (((x ^ y) & (x ^ r)) < 0) {
            logger.error("Ошибка в методе subtraction.");
            throw new ArithmeticException("integer overflow");
        }
        logger.debug("Успешный выход из метода subtraction. Возвращаемый результат: " + r);
        return r;
    }

    public int multiplication(int x, int y) {
        logger.debug("Вход метода multiplication. Аргументы: " + x + ", " + y);
        long r = (long)x * (long)y;
        if ((int)r != r) {
            logger.error("Ошибка в методе multiplication.");
            throw new ArithmeticException("integer overflow");
        }
        logger.debug("Успешный выход из метода multiplication. Возвращаемый результат: " + r);
        return (int) r;
    }

    public int division(int x, int y) throws ArithmeticException {
        logger.debug("Вход метода division. Аргументы: " + x + ", " + y);
        if (y == 0) {
            logger.error("Деление на 0.");
            throw new IllegalArgumentException("Division by zero.");
        }
        int r = x / y;
        logger.debug("Успешный выход из метода division. Возвращаемый результат: " + r);
        return r;
    }

    public double sqrt(int x) {
        logger.debug("Вход метода sqrt. Аргумент: " + x);
        double result = Math.sqrt(x);
        logger.debug("Успешный выход из метода sqrt. Возвращаемый результат: " + result);
        return result;
    }

    public boolean isPrime(int x) {
        logger.debug("Вход метода isPrime. Аргумент: " + x);
        boolean result = true;
        if (x < 2) {
            result = false;
        }
        double upLimit = sqrt(x);
        for (int i=2; i<upLimit; i++) {
            if (x % i == 0) {
                result = false;
            }
        }
        logger.debug("Успешный выход из метода isPrime. Возвращаемый результат: " + result);
        return result;
    }

    public int getFibonacci (int level) {
        logger.debug("Вход метода getFibonacci. Аргумент: " + level);
        int result = fibonacci(level);
        logger.debug("Успешный выход из метода getFibonacci. Возвращаемый результат: " + result);
        return result;
    }

    private int fibonacci (int level) {
        logger.trace("Вход метода fibonacci. Аргумент: " + level);
        if (level<0) {
            logger.error("Неправильные аргументы для метода getFibonacci.");
            throw new IllegalArgumentException("negative number");
        }
        if (level==0) {
            logger.trace("Успешный выход из метода fibonacci. Возвращаемый результат: 0");
            return 0;
        }
        if (level==1) {
            logger.trace("Успешный выход из метода fibonacci. Возвращаемый результат: 1");
            return 1;
        }
        int result = fibonacci(level-1) + fibonacci(level-2);
        logger.trace("Успешный выход из метода fibonacci. Возвращаемый результат: " + result);
        return result;
    }

    public int summarizeNumberRow(int[] input) {
        logger.debug("Вход метода summarizeNumberRow. Аргумент: " + input);
        int result = 0;
        for (int in : input) {
            result = addition(result, in);
        }
        logger.debug("Успешный выход из метода summarizeNumberRow. Возвращаемый результат: " + result);
        return result;
    }
}
