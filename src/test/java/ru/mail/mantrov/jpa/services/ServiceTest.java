package ru.mail.mantrov.jpa.services;

import org.apache.log4j.Logger;
import org.junit.*;
import ru.mail.mantrov.database.DBToolsForWork;
import ru.mail.mantrov.database.DBToolsForWorkTest;
import ru.mail.mantrov.jpa.pojo.Club;
import ru.mail.mantrov.jpa.pojo.Player;

import javax.persistence.Tuple;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;


public class ServiceTest {

    private Logger logger = Logger.getLogger(DBToolsForWorkTest.class);
    private static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties";
    private static String URL_DEFAULT;
    private static String DB_DEFAULT;

    private static DBToolsForWork db;
    private static Service service;

    @BeforeClass
    public static void connectBeforeTest() throws IOException {
        service = new Service();
        Properties propertiesDefault = new Properties();
        FileInputStream fis = new FileInputStream(PATH_TO_PROPERTIES);
        propertiesDefault.load(fis);
        URL_DEFAULT = propertiesDefault.getProperty("url");
        DB_DEFAULT = propertiesDefault.getProperty("db");

        try (Connection connection = DriverManager.getConnection(URL_DEFAULT.concat(DB_DEFAULT), propertiesDefault)) {
            DBToolsForWork dbToolsForWork = new DBToolsForWork(connection);
            dbToolsForWork.executeSQLScript("src/main/resources/jpa/postgres_data_for_jpa_homework.sql");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void deleteDB() throws IOException, SQLException, InterruptedException {
        Properties propertiesDefault = new Properties();
        FileInputStream fis = new FileInputStream(PATH_TO_PROPERTIES);
        propertiesDefault.load(fis);
        try (Connection connection = DriverManager.getConnection(URL_DEFAULT.concat(DB_DEFAULT), propertiesDefault)) {
            DBToolsForWork dbToolsForWork = new DBToolsForWork(connection);
            dbToolsForWork.executeSQLScript("src/main/resources/jpa/cleaner_jpa.sql");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void findPlayers() {
        List<Player> players = service.findAllPlayers();
        for (Player player : players) {
            logger.debug(player);
        }
    }

    @Test
    public void findAllPlayersFromCityJPQL() {
        logger.debug("Players from Moscow:");
        List<Player> players = service.findAllPlayersFromCityJPQL("Moscow");
        printPlayers(players);

        logger.debug("Players from Samara:");
        players = service.findAllPlayersFromCityJPQL("Samara");
        printPlayers(players);
    }

    @Test
    public void findAllPlayersFromCityNamedQuery() {
        logger.debug("Players from Moscow:");
        List<Player> players = service.findAllPlayersFromCityNamedQuery("Moscow");
        printPlayers(players);

        logger.debug("Players from Samara:");
        players = service.findAllPlayersFromCityNamedQuery("Samara");
        printPlayers(players);
    }

    @Test
    public void findAllPlayersFromMoscowNativeQuery() {
        logger.debug("Players from Moscow:");
        List<String> str = service.findAllPlayersFromMoscowNativeQuery();
        System.out.println(str);
    }

    @Test
    public void findAllPlayersFromCityCriteria() {
        logger.debug("Players from Moscow:");
        List<Player> players = service.findAllPlayersFromCityCriteria("Moscow");
        printPlayers(players);

        logger.debug("Players from Samara:");
        players = service.findAllPlayersFromCityCriteria("Samara");
        printPlayers(players);
    }

    @Test
    public void findAllClubsWithPlayers() {
        logger.debug("Clubs with 2 or more players:");
        List<Club> clubs = service.findAllClubsWithPlayers(2);
        printClubs(clubs);

        logger.debug("Clubs with 3 or more players:");
        clubs = service.findAllClubsWithPlayers(3);
        printClubs(clubs);
    }

    @Test
    public void findAllClubsWithPlayersJPQL() {
        logger.debug("Clubs with 1 or more players:");
        List<Club> clubs = service.findAllClubsWithPlayersJPQL(1);
        printClubs(clubs);

        logger.debug("Clubs with 3 or more players:");
        clubs = service.findAllClubsWithPlayersJPQL(3);
        printClubs(clubs);
    }

    @Test
    public void findAllClubsWithPlayersNativeQuery() {
        List<String> clubs = service.findAllClubsWithPlayersNativeQuery();
        logger.debug(clubs);
    }

    @Test
    public void findAllClubsWithPlayersCriteria() {
        logger.debug("Clubs with 1 or more players:");
        List<Tuple> result = service.findAllClubsWithPlayersCriteria(1);
        String outFormat = "Club title: %s, player count: %s";
        result.forEach(tuple -> logger.debug(String.format(outFormat, tuple.get(0), tuple.get(1, Long.class))));
    }

    private void printPlayers(List<Player> players) {
        for (Player player : players) {
            logger.debug(player);
        }
    }

    private void printClubs(List<Club> clubs) {
        for (Club club : clubs) {
            logger.debug(club);
        }
    }



}