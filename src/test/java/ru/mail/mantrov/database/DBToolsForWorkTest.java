package ru.mail.mantrov.database;

import org.apache.log4j.Logger;
import org.junit.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class DBToolsForWorkTest {
    Logger logger = Logger.getLogger(DBToolsForWorkTest.class);
    public static final String PATH_TO_PROPERTIES = "src/main/resources/config.properties";
    public static String URL_DEFAULT;
    public static String DB_DEFAULT;

    public static DBToolsForWork db;

    @BeforeClass
    public static void connectBeforeTest() throws IOException {
        Properties propertiesDefault = new Properties();
        FileInputStream fis = new FileInputStream(PATH_TO_PROPERTIES);
        propertiesDefault.load(fis);
        URL_DEFAULT = propertiesDefault.getProperty("url");
        DB_DEFAULT = propertiesDefault.getProperty("db");

        try (Connection connection = DriverManager.getConnection(URL_DEFAULT.concat(DB_DEFAULT), propertiesDefault)) {
            DBToolsForWork dbToolsForWork = new DBToolsForWork(connection);
            dbToolsForWork.executeSQLScript("src/main/resources/user.sql");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String url = URL_DEFAULT.concat("mantrovdb");

        Properties properties = new Properties();
        properties.setProperty("user", "Mantrov");
        properties.setProperty("password", "mantrov");
        properties.setProperty("ssl", "false");
        try {
            Connection connection = DriverManager.getConnection(url, properties);
            db = new DBToolsForWork(connection);
            db.executeSQLScript("src/main/resources/data_postgresql.sql");
            db.executeSQLScriptOverall("src/main/resources/check_player_from_kc.sql");
            db.executeSQLScriptOverall("src/main/resources/delete_player.sql");
            db.executeSQLScriptOverall("src/main/resources/trigger.sql");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void deleteDB() throws IOException, SQLException, InterruptedException {
        //Thread.sleep(60_000);
        db.connection.close();

        Properties propertiesDefault = new Properties();
        FileInputStream fis = new FileInputStream(PATH_TO_PROPERTIES);
        propertiesDefault.load(fis);
        try (Connection connection = DriverManager.getConnection(URL_DEFAULT.concat(DB_DEFAULT), propertiesDefault)) {
            DBToolsForWork dbToolsForWork = new DBToolsForWork(connection);
            dbToolsForWork.executeSQLScript("src/main/resources/cleaner.sql");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Before
    @After
    public void emptyLine() {
        System.out.println();
    }


    @Test
    public void executeQuery() throws SQLException {
        logger.debug("Список всех клубов и их игроков, сортированный по возрасту:");
        ResultSet rs = db.executeQuery("SELECT clubs.title, players.first_name, " +
                "players.second_name, players.birth_date, players.number FROM(" +
                "players INNER JOIN clubs ON players.club_id=clubs.id) ORDER BY players.birth_date DESC;");
        String outFormat = "%s\t%s %s, %s, number: %s.";
        while (rs.next()) {
            logger.info(String.format(outFormat, rs.getString(1),rs.getString(2),
                    rs.getString(3), rs.getDate(4), rs.getInt(5)));
        }
    }

    @Test
    public void getClubPlayers() throws SQLException {
        logger.debug("Вывод всех игроков команды Dinamo:");
        ResultSet rs = db.getClubPlayers("Dinamo");
        String outFormat = "%s %s, number:%s";

        while (rs.next()) {
            logger.info(String.format(outFormat, rs.getString(1), rs.getString(2), rs.getInt(3)));
        }
    }

    @Test
    public void deletePlayer() throws SQLException {
        logger.info("Удаление Еременко.");
        db.deletePlayer(11); // Delete Eremenko
        executeQuery();
    }

    @Test(expected = SQLException.class)
    public void deletePlayerKS() throws SQLException {
        logger.debug("Попытка удалить Соболева. Не должен удалиться, так как он - игрок крыльев.");
        db.deletePlayer(14);
        executeQuery();
    }

    @Test
    public void transferChange() throws SQLException {
        db.transferChange(1,3); // Change Morozov to Fernandes
        executeQuery();
    }

    @Test
    public void oldestPlayers() throws SQLException {
        logger.debug("Вывожу самых возрастных игроков из каждого города:");
        ResultSet rs = db.executeQuery("SELECT MIN(players.birth_date), cities.name FROM(" +
                "(players INNER JOIN clubs ON players.club_id=clubs.id) LEFT JOIN " +
                "cities ON clubs.city_id=cities.id) " +
                "GROUP BY cities.name");
        String outFormat = "%s, %s.";
        while (rs.next()) {
            logger.info(String.format(outFormat, rs.getDate(1), rs.getString(2)));
        }
    }
}