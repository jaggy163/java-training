package testing.ru.mail.mantrov;


import org.junit.Test;
import org.junit.runner.RunWith;

import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(NumberRowSupplier.class)
public class CalculatorPositiveTest {
    public static final double DELTA = 0.001;
    Calculator calculator = new Calculator();

    @Test
    public void addition() {
        assertEquals(25, calculator.addition(19,6));
        assertEquals(1_965_576, calculator.addition(1_263_732,701_844));
        assertEquals(-114, calculator.addition(78,-192));
    }

    @Test
    public void subtraction() {
        assertEquals(19, calculator.subtraction(25,6));
        assertEquals(1_263_732, calculator.subtraction(1_965_576,701_844));
        assertEquals(78, calculator.subtraction(-114,-192));
    }

    @Test
    public void multiplication() {
        assertEquals(56, calculator.multiplication(7,8));
        assertEquals(544, calculator.multiplication(-17,-32));
        assertEquals(-594, calculator.multiplication(-11,54));
    }

    @Test
    public void division() {
        assertEquals(7, calculator.division(56,8));
        assertEquals(-17, calculator.division(544,-32));
        assertEquals(54, calculator.division(-594,-11));
        assertEquals(-1, calculator.division(-4, 3));
    }

    @Test
    public void sqrt() {
        assertEquals(5, calculator.sqrt(25), DELTA);
        assertEquals(17, calculator.sqrt(289), DELTA);
        assertTrue(Double.isNaN(calculator.sqrt(-5)));
    }

    @Test(timeout = 1000)
    public void isPrime() {
        assertTrue(calculator.isPrime(7));
        assertTrue(calculator.isPrime(757));
        assertTrue(calculator.isPrime(1_002_289));
        assertFalse(calculator.isPrime(24));
        assertFalse(calculator.isPrime(-7));
    }

    @Test
    public void getFibonacci() {
        assertEquals(0, calculator.getFibonacci(0));
        assertEquals(1, calculator.getFibonacci(1));
        assertEquals(1, calculator.getFibonacci(2));
        assertEquals(2, calculator.getFibonacci(3));
        assertEquals(13, calculator.getFibonacci(7));
        assertEquals(10946, calculator.getFibonacci(21));
    }

    @Test
    public void summarizeNumberRow() throws Exception {
        assertEquals(6, calculator.summarizeNumberRow(NumberRowSupplier.staticRow()));
        assertEquals(15, calculator.summarizeNumberRow(new NumberRowSupplier().finalRow()));
        int[] rowForFinal = {10,20,30};
        int[] rowForStatic = {10,10,10};
        int[] rowForPrivate = {20,15,0};
        int[] rowCheck = {3,4,3};

        assertEquals(10, calculator.summarizeNumberRow(rowCheck));

        NumberRowSupplier numberRowSupplier = mock(NumberRowSupplier.class);
        when(numberRowSupplier.finalRow()).thenReturn(rowForFinal);
        assertEquals(60, calculator.summarizeNumberRow(numberRowSupplier.finalRow()));

        spy(NumberRowSupplier.class);
        when(NumberRowSupplier.staticRow()).thenReturn(rowForStatic);
        assertEquals(30, calculator.summarizeNumberRow(NumberRowSupplier.staticRow()));

        NumberRowSupplier numberRowSupplier1 = spy(new NumberRowSupplier());
        when(numberRowSupplier1, "privateRow").thenReturn(rowForPrivate);
        int[] row = numberRowSupplier1.getPrivateRow();
        assertEquals(35, calculator.summarizeNumberRow(row));
    }
}