package testing.ru.mail.mantrov;

import org.junit.Test;


public class CalculatorNegativeTest {
    Calculator calculator = new Calculator();

    @Test(expected = ArithmeticException.class)
    public void addition() {
        calculator.addition(1_500_000_000, 1_200_000_000);
    }

    @Test(expected = ArithmeticException.class)
    public void subtraction() {
        calculator.subtraction(-1_500_000_000, 1_200_000_000);
    }

    @Test(expected = ArithmeticException.class)
    public void multiplication() {
        calculator.multiplication(1_000_000_000, 32);
    }

    @Test(expected = IllegalArgumentException.class)
    public void division() {
        calculator.division(54, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFibonacci() {
        calculator.getFibonacci(-4);
    }

}