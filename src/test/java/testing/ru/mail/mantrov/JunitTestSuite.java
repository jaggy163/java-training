package testing.ru.mail.mantrov;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculatorNegativeTest.class,
        CalculatorPositiveTest.class
})

public class JunitTestSuite {
}
