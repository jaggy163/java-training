package ru.mail.mantrov.payment;

import ru.mail.mantrov.payment.terminal.TerminalFacade;

public class Main {

    public static void main(String[] args) {
        TerminalFacade.paymentWithCard(500, 89279092281l);

        TerminalFacade.paymentWithCard(40, 89279092281l);

        TerminalFacade.paymentWithCash(150, 200, 89279092281l);

        TerminalFacade.paymentWithCash(200, 500, 89179716256l);
    }
}
