package ru.mail.mantrov.payment.terminal;

public enum PaymentType {
    CARD_PAYMENT, CASH_PAYMENT
}
