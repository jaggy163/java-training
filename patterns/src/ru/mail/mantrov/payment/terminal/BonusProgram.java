package ru.mail.mantrov.payment.terminal;

import java.util.HashMap;
import java.util.Map;

public class BonusProgram {
    private final Map<Long, ClientBonus> clientMap = new HashMap<>();
    private final static float PERCENT = 0.1f;
    private final static BonusProgram instance = new BonusProgram();


    private BonusProgram() {

    }

    public static BonusProgram getInstance() {
        return instance;
    }

    // PaymentType ввел и оставил на случай, если потребуются разные виды бонусных систем для разных способов оплат.
    public float tryToWriteOffBonuses(float orderPrice, long telephoneNumber, PaymentType type) {
        int bonuses = checkBonuses(telephoneNumber);
        float price = writeOffBonuses(telephoneNumber, orderPrice, bonuses);
        int addedBonuses = addBonuses(telephoneNumber, price);
        System.out.println("Бонусной системой списано " + (orderPrice-price) + ".\nБонусной системой начислено " + addedBonuses);
        return price;
    }

    private int checkBonuses(long telephoneNumber) {
        if (!clientMap.containsKey(telephoneNumber)) {
            clientMap.put(telephoneNumber, new ClientBonus());
        }
        return clientMap.get(telephoneNumber).getBonuses();
    }

    private int addBonuses(long telephoneNumber, float orderPrice) {
        int bonuses = (int)(orderPrice*PERCENT);
        clientMap.get(telephoneNumber).giveBonuses(bonuses);
        return bonuses;
    }

    private float writeOffBonuses(long telephoneNumber, float orderPrice, int bonuses) {
        if (bonuses > orderPrice) {
            clientMap.get(telephoneNumber).writeOffBonuses(Math.round(orderPrice));
            return 0f;
        }
        clientMap.get(telephoneNumber).writeOffBonuses(bonuses);
        return orderPrice-bonuses;
    }
}
