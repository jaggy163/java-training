package ru.mail.mantrov.payment.terminal;

public class TerminalFacade {
    private static Terminal terminal = new Terminal();

    public static void paymentWithCard(float orderPrice, long telephoneNumber) {
        terminal.paymentWithCard(orderPrice, telephoneNumber);
    }

    public static void paymentWithCash(float orderPrice, float cash, long telephoneNumber) {
        terminal.paymentWithCash(orderPrice, cash, telephoneNumber);
    }
}
