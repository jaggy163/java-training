package ru.mail.mantrov.payment.terminal;

import java.util.HashMap;
import java.util.Map;

class Terminal {

    protected void paymentWithCard(float orderPrice, long telephoneNumber) {
        System.out.println("Клиент с номером телефона " + telephoneNumber + ".\nЗаказ на сумму: " + orderPrice + ".");

        BonusProgram bonusProgram = BonusProgram.getInstance();
        float priceAfterBonuses = bonusProgram.tryToWriteOffBonuses(orderPrice, telephoneNumber, PaymentType.CARD_PAYMENT);

        System.out.println("После списания бонусов к оплате: " + priceAfterBonuses +
                ".\nОплата произведена картой.\n");
    }

    protected void paymentWithCash(float orderPrice, float cash, long telephoneNumber) {
        System.out.println("Клиент с номером телефона " + telephoneNumber + ".\nЗаказ на сумму: " + orderPrice + ".");

        BonusProgram bonusProgram = BonusProgram.getInstance();
        float priceAfterBonuses = bonusProgram.tryToWriteOffBonuses(orderPrice, telephoneNumber, PaymentType.CASH_PAYMENT);
        if (giveChange(cash, priceAfterBonuses)) {
            System.out.println("После списания бонусов сумма к оплате составила " + priceAfterBonuses +
                    ".\nОплата произведена наличными средствами.\n");
        }
    }




    private boolean giveChange(float cash, float orderPrice) {
        if (cash<orderPrice) {
            System.out.println("Внесена недостаточная сумма. В оплате отказано.");
            return false;
        }
        float change = cash - orderPrice;
        System.out.println("Оплата произведена наличными, в кассу внесено: " + cash + ", сдача составила: " + change + ".");
        return true;
    }
}