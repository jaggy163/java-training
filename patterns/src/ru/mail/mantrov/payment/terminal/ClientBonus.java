package ru.mail.mantrov.payment.terminal;

class ClientBonus {
    private int bonuses=0;

    protected void giveBonuses(int givenBonuses) {
        bonuses += givenBonuses;
    }

    protected void writeOffBonuses(int writtenOffBonuses) {
        if (writtenOffBonuses > bonuses) {
            throw new IllegalArgumentException();
        }
        bonuses -= writtenOffBonuses;
    }

    protected int getBonuses() {
        return bonuses;
    }
}
