package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.Food;
import ru.mail.mantrov.restaurant.food.FoodMenu;
import ru.mail.mantrov.restaurant.food.OnTheBar;
import ru.mail.mantrov.restaurant.food.OnTheKitchen;

import java.util.ArrayList;
import java.util.List;

public class Waiter {
    private final Order order;

    public Waiter(Order order) {
        this.order = order;
    }

    public List<Food> work() {
        List<FoodMenu> list = order.getList();
        List<Food> resultList = new ArrayList<>();
        for(FoodMenu foodMenu : list) {
            if (foodMenu instanceof OnTheBar) {
                resultList.add(Bar.createDrink((OnTheBar) foodMenu));
            } else if (foodMenu instanceof OnTheKitchen) {
                resultList.add(Kitchen.getInstance().createPizza((OnTheKitchen)foodMenu));
            }
        }
        return resultList;
    }
}
