package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.OnTheKitchen;
import ru.mail.mantrov.restaurant.food.Pizza;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

import java.util.Random;

public class Kitchen {
    private final boolean isPizzaMakerHired = new Random().nextBoolean();
    private final PizzaProducer pizzaProducer;
    private static final Kitchen instance = new Kitchen();

    private Kitchen() {
        if (isPizzaMakerHired) {
            System.out.println("Kitchen is opened. PizzaMaker is hired.");
            pizzaProducer = new PizzaMaker();
        } else {
            System.out.println("Kitchen is opened. PizzaMaker is not hired");
            pizzaProducer = new PizzaShop();
        }
    }

    public static Kitchen getInstance() {
        return instance;
    }

    public Pizza createPizza(OnTheKitchen choice) {
        if (choice instanceof PizzaMenu) {
            return pizzaProducer.createPizza((PizzaMenu) choice);
        }
        return null;
    }
}
