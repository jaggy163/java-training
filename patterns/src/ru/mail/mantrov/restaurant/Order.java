package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.DrinkMenu;
import ru.mail.mantrov.restaurant.food.FoodMenu;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private final PizzaMenu pizza;
    private final DrinkMenu drink;
    private final List<FoodMenu> list = new ArrayList<>();

    private Order(OrderBuilder builder) {
        this.pizza = builder.pizza;
        this.drink = builder.drink;
        list.add(pizza);
        list.add(drink);
    }

    public DrinkMenu getDrink() {
        return drink;
    }

    public PizzaMenu getPizza() {
        return pizza;
    }

    public List<FoodMenu> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Order{" +
                "pizza=" + pizza +
                ", drink=" + drink +
                '}';
    }

    public static class OrderBuilder {
        private PizzaMenu pizza;
        private DrinkMenu drink;

        public OrderBuilder choosePizza(PizzaMenu pizza) {
            this.pizza = pizza;
            return this;
        }

        public OrderBuilder chooseDrink(DrinkMenu drink) {
            this.drink = drink;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }
}
