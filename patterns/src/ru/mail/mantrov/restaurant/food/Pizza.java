package ru.mail.mantrov.restaurant.food;

public class Pizza implements Food {
    private final PizzaMenu chosen;
    public Pizza(PizzaMenu chosen) {
        this.chosen = chosen;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "chosen=" + chosen +
                '}';
    }
}
