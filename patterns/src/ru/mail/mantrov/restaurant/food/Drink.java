package ru.mail.mantrov.restaurant.food;

public class Drink implements Food {
    private final DrinkMenu chosen;

    public Drink(DrinkMenu chosen) {
        this.chosen = chosen;
    }

    @Override
    public String toString() {
        return "Drink{" +
                "chosen=" + chosen +
                '}';
    }
}

