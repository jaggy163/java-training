package ru.mail.mantrov.restaurant.food;

public enum DrinkMenu implements OnTheBar {
    COFFEE, TEA, LEMONADE, BEER
}
