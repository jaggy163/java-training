package ru.mail.mantrov.restaurant.food;

public enum PizzaMenu implements OnTheKitchen {
    MARGARITA, FOUR_CHEESE, PEPPERONI
}
