package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.Pizza;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

public class PizzaShop implements PizzaProducer {
    @Override
    public Pizza createPizza(PizzaMenu pizzaChoice) {
        return buyPizza(pizzaChoice);
    }

    public Pizza buyPizza(PizzaMenu pizzaChoice) {
        System.out.println("WaiterImpl bought the pizza " + pizzaChoice + " in the PizzaShop");
        return new Pizza(pizzaChoice);
    }
}
