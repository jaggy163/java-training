package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.DrinkMenu;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

import java.util.List;

public class Client {

    public void makeAnOrder(DrinkMenu drinkChoice, PizzaMenu pizzaChoice) {
        System.out.println("Client make an order " + pizzaChoice + " and " + drinkChoice);
        Order order = new Order.OrderBuilder()
                .chooseDrink(drinkChoice)
                .choosePizza(pizzaChoice)
                .build();
        Waiter waiter = new Waiter(order);
        List list = waiter.work();
        System.out.println("Клиент получил: " + list + "\n");
    }
}
