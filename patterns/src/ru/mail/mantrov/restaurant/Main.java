package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.DrinkMenu;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        client.makeAnOrder(DrinkMenu.BEER, PizzaMenu.PEPPERONI);

        client.makeAnOrder(DrinkMenu.COFFEE, null);

        client.makeAnOrder(null, PizzaMenu.FOUR_CHEESE);
    }
}
