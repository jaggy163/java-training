package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.Pizza;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

public interface PizzaProducer {
    public Pizza createPizza(PizzaMenu pizzaChoice);

}
