package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.Pizza;
import ru.mail.mantrov.restaurant.food.PizzaMenu;

public class PizzaMaker implements PizzaProducer {
    @Override
    public Pizza createPizza(PizzaMenu pizzaChoice) {
        System.out.println("PizzaMaker made " + pizzaChoice);
        return makePizza(pizzaChoice);
    }

    private Pizza makePizza(PizzaMenu pizzaChoice) {
        return new Pizza(pizzaChoice);
    }

}
