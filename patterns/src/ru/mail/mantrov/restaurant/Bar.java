package ru.mail.mantrov.restaurant;

import ru.mail.mantrov.restaurant.food.Drink;
import ru.mail.mantrov.restaurant.food.DrinkMenu;
import ru.mail.mantrov.restaurant.food.OnTheBar;

public class Bar {
    public static Drink createDrink(OnTheBar choice) {
        if (choice instanceof DrinkMenu) {
            System.out.println("Drink " + choice + " is ready.");
            return new Drink((DrinkMenu)choice);
        }
        return null;
    }
}
