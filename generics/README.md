## Java Generics

Есть класс `homework.Mantrov.Order` - это класс заказа. Класс `homework.Mantrov.SpecificOrder` - 
его наследник, который помечает время создания заказа. `homework.Mantrov.OrderProcessor` - обработчик
заказов, он переводит заказ из статуса `NOT_STARTED` в `PROCESSING` и через какое-то время в `COMPLETED`,
Он может работать с `Order` и всеми его наследниками. `homework.Mantrov.MainGeneric` - создает несколько
заказов `Order` и `SpecificOrder` и запускает на каждый из них свой обработчик.