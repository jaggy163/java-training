package ru.mail.mantrov;

import java.text.DecimalFormat;
import java.util.Objects;

public class Order implements Comparable<Order> {
    private final static java.util.Random ID_SEQUENCE;

    static {
        ID_SEQUENCE = new java.util.Random(999l);
    }

    private final Long id;
    private OrderStatus status;
    private final int items;
    private final double price;

    {
        this.id = ID_SEQUENCE.nextLong();
    }

    public Order(OrderStatus status, int items, double price) {
        this.status = status;
        this.items = items;
        this.price = price;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public int getItems() {
        return items;
    }

    public double getPrice() {
        return price;
    }

    public Long getId() {
        return id;
    }

    public void changeStatus(OrderStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id.equals(order.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Order o) {
        if (this == null && o == null) {
            return 0;
        } else if (this == null) {
            return -1;
        } else if (o == null) {
            return 1;
        } else {
            return (int) (this.getId() - o.getId());
        }
    }

    @Override
    public String toString() {
        String formattedDouble = new DecimalFormat("#0.00").format(price);
        return "Order{" +
                "status=" + status +
                ", items=" + items +
                ", price=" + formattedDouble +
                '}';
    }
}