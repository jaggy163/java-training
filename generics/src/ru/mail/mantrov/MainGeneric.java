package ru.mail.mantrov;

import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;

public class MainGeneric {
    public static final Queue<Order> nonStartedOrders = new ConcurrentLinkedQueue();

    public static void main(String[] args) throws InterruptedException {

        for (int i=0; i<100; i++) {
            if (i%2==0) {
                SpecificOrder order = new SpecificOrder(OrderStatus.NOT_STARTED, (int) (100 * Math.random()), (500 * Math.random()));
                nonStartedOrders.add(order);
            } else {
                Order order = new Order(OrderStatus.NOT_STARTED, (int) (100 * Math.random()), (500 * Math.random()));
                nonStartedOrders.add(order);
            }
        }
        int threadsNumber = Runtime.getRuntime().availableProcessors();
        for (int i=0; i<threadsNumber; i++) {
            new Thread(new OrderProcessor<>()).start();
        }
    }
}
