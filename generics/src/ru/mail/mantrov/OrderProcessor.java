package ru.mail.mantrov;

public class OrderProcessor<O extends Order> implements Runnable {
    private O order;

    @Override
    public void run() {
        while (!MainGeneric.nonStartedOrders.isEmpty()) {
            try {
                order = (O) MainGeneric.nonStartedOrders.poll();
                process();
                Thread.sleep((int) (Math.random() * 8000));
                close();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void process() {
        order.changeStatus(OrderStatus.PROCESSING);
        System.out.println(order + " переведен в работу");
    }

    private void close() throws InterruptedException {
        order.changeStatus(OrderStatus.COMPLETED);
        System.out.println(order + " обработан");
    }
}
