package ru.mail.mantrov;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SpecificOrder extends Order {
    public final Date creatingDate;

    public SpecificOrder(OrderStatus status, int items, double price) {
        super(status, items, price);
        creatingDate = new Date();
    }

    public Date getCreatingDate() {
        return creatingDate;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return super.toString() + " [creatingDate: " + formatter.format(creatingDate) + "]";
    }
}
