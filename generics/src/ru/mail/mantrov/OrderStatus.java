package ru.mail.mantrov;

public enum OrderStatus {
    NOT_STARTED, PROCESSING, COMPLETED
}
