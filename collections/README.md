# Java Collections

## Getting started

1. Open `io.humb1t.Main.java` and compare different approaches in iterations. Write down your thoughts,
what is your favourite option? Which one is easier to read and which one is easier to write? Why?
1. Add numeric field to `Order` class, use it to filter collection of orders by some criteria 
(more than 50 order items for example).
1. Imagine the situation - you need to implement [queue](https://en.wikipedia.org/wiki/Queue_(abstract_data_type\))
of incoming requests, to process incoming requests we have 3 independent "processors".
How can we design our application in such a case and what pros and cons we would face in different approaches?
Write down your thoughts and implement one possible solution.
1. Imagine the situation when you have a `Collection` and should remove all duplicates in it. 
How would you do it? Implement your solution using only Java SE.
1. There is an old war between `ArrayList` and `Linked List` - choose new fighters and implement your
own benchmark (`Vector` vs `Queue` for example). Write down your thoughts.
1. `Map` is very good in implementation of simple [caches](https://en.wikipedia.org/wiki/Cache_(computing\)).
Implement your own cache using `Map`.


## Comments

1. My favourite option is for-each. But, in my opinion, its bad if we have to remove some elements from collection
during iteration. In this case I'll pick `Iterator`. But I prefer `Iterator<Order> iterator = orders.iterator();`
and `while(iterator.hasNext()) { ... }`.
1. Done. `io.humb1t.Main.java`.
1. I think we should use `ConcurrentLinkedQueue`. I tried to implement my idea. `ru.mail.mantrov.requests.IncomingRequestQueue`
is a singleton with queue and methods to write and read requests. `ru.mail.mantrov.requests.RequestSender` is a class
to write in a queue. `ru.mail.mantrov.requests.Processor` is a class to read from a queue. `ru.mail.mantrov.requests.Main`
creates 1 thread with sender and 3 threads with processors. The minus of this implementation is that sender can write faster
then processors do useful things with requests and I don't control the size of a queue. If this situation is possible, we
should control the size of a queue and start new processors if necessary. In this case `ConcurrentLinkedQueue` could be
bad choice, because `size()` is not recommended for `ConcurrentLinkedQueue`.
1. Done. `ru.mail.mantrov.RemovingDuplicatesApp`.
1. `ru.mail.mantrov.BenchmarkVectorDueue`. I compared Vector and ArrayDeque. As expected the deque won in things it can do.
Vector and deque were both pretty good adding elements at the end of the collection, removing elements at the end and getting
elements from either side. In moments where I work with the beginning of the collection (remove, add) deque was much
better. This collections are both bad in 'contains' and 'remove from the middle' because queue is always bad in it and
vector was unsorted. So in conclusion, if we should to pick between `Vector` and `ArrayDeque` and we are satisfied with
the functionality of the deque, we should pick `ArrayDeque`. If we need some methods that are not available in Deque 
(if we want to work with the middle of the collection, not only sides), `Vector` is our choice.
1. Done. `ru.mail.mantrov.cache` - some kind of LRU cache implementation.
I wrote some comments inside this class to explain what I did. This is some test usage of the cache: `ru.mail.mantrov.test`.

