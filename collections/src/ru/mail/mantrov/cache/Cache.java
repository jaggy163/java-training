package ru.mail.mantrov.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * Ссылки на объекты, добавляемые в кэш хранятся в Map. Помимо этого из этих ссылок создается двунаправленная очередь, чтобы
 * оперировать порядком в кэше. Новые элементы помещаются в начало очереди (голова), выталкивая элементы в конце (хвост),
 * если кэш заполнен (размер кэша задается конструктором). Если добавляется или забирается из кэша элемент, который там уже
 * есть, то этот элемент перемещается в начало очереди.
 * @param <K> the type of keys
 * @param <V> the type of mapped values
 */

public class Cache<K, V> {
    private final int capacity;
    private final Map<K, Node<K, V>> cache;
    private Node<K, V> head;
    private Node<K, V> tail;

    public Cache (int capacity) {
        this.capacity = capacity;
        cache = new HashMap(capacity+1);
    }


    // Добавление нового элемента в кэш
    public void add(K key, V value) {
        if (cache.containsKey(key)) { // Если элемент уже есть в кэше, то перемещаем его в голову очереди.
            Node<K, V> node = cache.get(key);
            moveToHead(node);
            return;
        }
        if (cache.size() >= capacity) { // Если кэш заполнен, то удаляем последний объект в кэше.
            removeTail();
        }
        Node<K, V> node = new Node<>(key, value);
        newHead(node);
        cache.put(key, node);
    }


    // Доставание элемента из кэша. Если элемента нет - возвращаем null. Если есть - возвращаем элемент и перемещаем его в начало очереди.
    public V get(K key) {
        if (cache.containsKey(key)) {
            Node<K, V> node = cache.get(key);
            moveToHead(node);
            return node.value;
        }
        return null;
    }


    // Вырываем элемент из очереди, зачищаем ссылки на него и перемещаем в голову.
    private void moveToHead(Node<K, V> node) {
        if (node==head) {
            return;
        }
        node.previous.next = node.next;
        if (node!=tail) {
            node.next.previous = node.previous;
        } else {
            tail = node.previous;
        }
        newHead(node);
    }


    // Перемещение элемента в голову
    private void newHead(Node<K, V> node) {
        if (head != null) {
            head.previous = node;
        }
        node.next = head;
        node.previous = null;
        head = node;
        if (tail==null) {
            tail = node;
        }
    }


    private void removeTail() {
        cache.remove(tail.key);
        tail = tail.previous;
        tail.next = null;
    }


    // Очищение кэша.
    public void clear() {
        head = null;
        tail = null;
        cache.clear();
    }


    // Внутренний класс для создания двунаправленной очереди.
    private class Node<K, V> {
        K key;
        V value;
        Node previous;
        Node next;
        private Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return key + "=" + value;
        }
    }

    @Override
    public String toString() {
        return "[" + stringValueOfAll(head);
    }

    private String stringValueOfAll(Node<K, V> node) {
        if (node==null) {
            return "]";
        }
        if (node.next == null) {
            return node.toString() + "]";
        }
        return node.toString() + ", " + stringValueOfAll(node.next);
    }
}
