package ru.mail.mantrov.cache;

public class TestCache {
    public static void main(String[] args) {
        Cache<Integer, String> cache = new Cache<>(4);
        System.out.println("Добавили 4 элемента в кэш");
        cache.add(1, "one");
        cache.add(2, "two");
        cache.add(3, "three");
        cache.add(4, "four");
        System.out.println(cache);
        System.out.println();

        System.out.println("Достаем из кэша 1");
        System.out.println(cache.get(1));
        System.out.println(cache);
        System.out.println();

        System.out.println("Достаем из кэша 2");
        System.out.println(cache.get(2));
        System.out.println(cache);
        System.out.println();

        System.out.println("Добавляем в кэш пятый элемент. Должен вытесниться последний, на данный момент элемент кэша");
        cache.add(5, "five");
        System.out.println(cache);
        System.out.println();

        System.out.println("Пытаемся достать из кэша 3");
        System.out.println(cache.get(3));
        System.out.println();

        System.out.println("Достаем из кэша элемент 4");
        System.out.println(cache.get(4));
        System.out.println(cache);
        System.out.println();

        System.out.println("Добавляем в кэш элемент 6");
        cache.add(6, "six");
        System.out.println(cache);
        System.out.println();

        System.out.println("Пытаемся достать из кэша 1");
        System.out.println(cache.get(1));
        System.out.println(cache);

    }
}
