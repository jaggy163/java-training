package ru.mail.mantrov;

import java.util.*;

public class RemovingDuplicatesApp {
    static Collection<Integer> list;

    public static void main(String[] args) {
        list = new ArrayList<>();
        for (int i=0; i<30; i++) {
            list.add((int)(Math.random()*10));
        }
        System.out.println("Лист с дубликатами\n" + list);
        list = removeDuplicates();
        System.out.println("Лист без дубликатов:\n" + list);
    }

    public static Collection removeDuplicates() {
        return new HashSet(list);
    }
}
