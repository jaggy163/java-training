package ru.mail.mantrov;

import java.util.ArrayDeque;
import java.util.Vector;

public class BenchmarkVectorDueue {
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        long start, finish;

        System.out.println("Добавление элементов в конец (1_000_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<1_000_000; i++) {
            vector.add((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<1_000_000; i++) {
            deque.add((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Добавление элементов в начало (10_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.add(0, (int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.addFirst((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Удаление элементов с конца (10_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.remove(vector.size()-1);
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.pollLast();
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Удаление элементов с начала (10_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.remove(0);
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.pollFirst();
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Проверка на наличие элемента (10_000 элементов):");
        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.contains((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.contains((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Получение элемента из коллекции без удаления по индексу (10_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.get((int) (Math.random()*vector.size()));
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.peek();
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


        System.out.println("Удаление первого совпадения (10_000 элементов):");

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            vector.removeElement((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("Vector:\t\t" + (finish-start));

        start = System.currentTimeMillis();
        for (int i=0; i<10_000; i++) {
            deque.remove((int) (Math.random()*1_000_000));
        }
        finish = System.currentTimeMillis();
        System.out.println("ArrayDeque:\t" + (finish-start));


    }
}
