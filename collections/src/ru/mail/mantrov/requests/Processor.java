package ru.mail.mantrov.requests;

class Processor extends Thread {
    @Override
    public void run() {
        IncomingRequestQueue queue = IncomingRequestQueue.getInstance();
        while (true) {
            try {
                doSomethingWithRequest(queue.getRequestToProcess());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void doSomethingWithRequest(String request) throws InterruptedException {
        if (request==null) {
//            Thread.sleep(1000);
            return;
        }
        // Doing smth useful
        System.out.println(request + "\tProcessed by " + Thread.currentThread());
//        Thread.sleep(120);
    }
}
