package ru.mail.mantrov.requests;

import java.util.Random;

class RequestSender extends Thread {
    private final static java.util.Random REQUEST_RANDOM = new Random(System.currentTimeMillis());

    @Override
    public void run() {
        IncomingRequestQueue queue = IncomingRequestQueue.getInstance();
        while (true) {
            queue.addNewRequest(sendSomething(REQUEST_RANDOM, "ABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890", 20));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static String sendSomething(Random rng, String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }
}
