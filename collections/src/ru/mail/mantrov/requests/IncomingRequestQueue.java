package ru.mail.mantrov.requests;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class IncomingRequestQueue {
    private static IncomingRequestQueue instance = new IncomingRequestQueue();
    private final Queue<String> requestQueue;

    private IncomingRequestQueue() {
        requestQueue = new ConcurrentLinkedQueue();
    }

    public static synchronized IncomingRequestQueue getInstance() {
        return instance;
    }

    public String getRequestToProcess() {
        return requestQueue.poll();
    }

    public void addNewRequest(String req) {
        requestQueue.add(req);
    }
}
