package io.humb1t;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Collection<String> c = Collections.EMPTY_LIST;
        List<String> list = new ArrayList<>(c);

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(OrderStatus.COMPLETED, 100));
        orders.add(new Order(OrderStatus.COMPLETED, 43));
        orders.add(new Order(OrderStatus.PROCESSING, 67));

        System.out.println("Stream:");
        orders.stream()
                .filter(order -> order.status == OrderStatus.COMPLETED)
                .filter(order -> order.items >= 50)
                .forEach(order -> System.out.println(order.toString()));

        System.out.println("For-each:");
        for (Order order : orders) {
            if (order.items >= 50) {
                System.out.println(order.toString());
            }
        }

        System.out.println("Iterator:");
        for (Iterator<Order> iterator = orders.iterator(); iterator.hasNext();){
            Order order = iterator.next();
            if (order.items >= 50) {
                System.out.println(order);
            }
        }

        System.out.println("Map:");
        Map<OrderStatus, List<Order>> ordersByStatus = orders.stream()
                .collect(Collectors.groupingBy(Order::getStatus));
        System.out.println(ordersByStatus.get(OrderStatus.COMPLETED));
    }


    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    public static class Order {
        public final OrderStatus status;
        private final int items;

        public Order(OrderStatus status, int items) {
            this.status = status;
            this.items = items;
        }

        public OrderStatus getStatus() {
            return status;
        }

        public int getItems() {
            return items;
        }

        @Override
        public String toString() {
            return "Order{" +
                    "status=" + status +
                    ", items=" + items +
                    '}';
        }
    }
}
