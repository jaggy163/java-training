package ru.mail.mantrov.bank.concurrency;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    static Lock lock;
    public static void main(String[] args) throws InterruptedException {
        lock = new ReentrantLock();
        int counter = 0;

        while (true) {
            System.out.println(counter++);
            Bank bank = new Bank(10_000);
            for (int i = 0; i < 5; i++) {
                Thread t = new Thread(new BankUser(bank));
                t.start();
            }
            Thread.sleep(100);
        }
    }
}
