package ru.mail.mantrov.bank.concurrency;

public class Bank {
    private int moneyAmount;

    public Bank(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public void transferMoney(int amount) {
        moneyAmount -= amount;
        //System.out.println(Thread.currentThread() + " Снятие с банка " + amount + ".\nОсталось " + moneyAmount + ".\n");
        if (moneyAmount<0) throw new RuntimeException();
    }

    public boolean hasMoney(int amount) {
        //System.out.println(Thread.currentThread() + " hasMoney с аргументами: " +amount + ", " + moneyAmount + (moneyAmount >= amount));
        return moneyAmount >= amount;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "moneyAmount=" + moneyAmount +
                '}';
    }
}
