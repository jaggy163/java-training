package ru.mail.mantrov.bank.concurrency;

import static java.lang.Thread.sleep;

public class BankUser implements Runnable {
    private final Bank bank;

    public BankUser(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void run() {
        int money = (int)(Math.random() * 100);
        try {
            while (true) {
                if (Main.lock.tryLock()) {
                    if (bank.hasMoney(money)) {
                        bank.transferMoney(money);
                    } else {
                        break;
                    }
                    Main.lock.unlock();
                }
                sleep(5);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            Main.lock.unlock();
        }
    }
}
