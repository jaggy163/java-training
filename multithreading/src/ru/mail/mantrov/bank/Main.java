package ru.mail.mantrov.bank;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int counter = 0;

        while (true) {
            System.out.println(counter++);
            Bank bank = new Bank(10_000);
            for (int i = 0; i < 5; i++) {
                Thread t = new Thread(new BankUser(bank));
                t.start();
            }
            Thread.sleep(100);
        }
    }
}
