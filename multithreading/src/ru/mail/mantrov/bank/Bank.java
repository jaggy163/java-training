package ru.mail.mantrov.bank;

public class Bank {
    private int moneyAmount;

    public Bank(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public void transferMoney(int amount) {
        moneyAmount -= amount;
        //System.out.println("Снятие с банка " + amount + ".\nОсталось " + moneyAmount + ".\n");
        if (moneyAmount<0) throw new RuntimeException();
    }

    public boolean hasMoney(int amount) {
        return moneyAmount >= amount;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "moneyAmount=" + moneyAmount +
                '}';
    }
}
