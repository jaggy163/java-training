package ru.mail.mantrov.bank;

import java.util.Random;

import static java.lang.Thread.sleep;

public class BankUser implements Runnable {
    private final Bank bank;

    public BankUser(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void run() {
        int money = (int)(Math.random() * 100);
        while (true) {
            synchronized (bank) {
                if (bank.hasMoney(money)) {
                    bank.transferMoney(money);
                } else {
                    break;
                }
            }

            try {
                sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
