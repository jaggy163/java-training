package ru.mail.mantrov;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class PasswordChecker implements Runnable {
    private int threadsNumber;
    private int currentThreadNumber;
    private int length = 1;

    String lastPassword = "";
    long counter = 0;

    public PasswordChecker(int threadsNumber, int currentThreadNumber) {
        this.threadsNumber = threadsNumber;
        this.currentThreadNumber = currentThreadNumber;
    }

    @Override
    public void run() {

        while (MainTask3.answer==null) {
            generate(length);
            length++;
            if (length>MainTask3.MAX_PASSWORD_LENGTH) {
                break;
            }
        }
    }

    public void generate(int length) {
        char[] chars = new char[length];

        final long MAX_WORDS = (long) Math.pow(MainTask3.symbols.size(), length);
        final long SECTOR_START = MAX_WORDS/threadsNumber * currentThreadNumber;
        final long SECTOR_END = MAX_WORDS/threadsNumber * (currentThreadNumber+1);
        for (long wordNumber = SECTOR_START; wordNumber < SECTOR_END; wordNumber++) {
            if (MainTask3.answer==null) {
                counter++;
                for (int i = 0; i < chars.length; i++) {
                    chars[i] = MainTask3.symbols.get((int) ((wordNumber / (Math.pow(26, i))) % 26));
                }
                lastPassword = String.valueOf(chars);
                String hash = hash(lastPassword);
                if (hash.hashCode() == MainTask3.hashCode.hashCode()) {
                    if (hash.equals(MainTask3.hashCode)) {
                        MainTask3.answer = lastPassword;
                    }
                }
            } else {
                System.out.println("Поток " + Thread.currentThread() + " обработал " + counter + " паролей. Последний: " + lastPassword);
                break;
            }
        }
    }

    @SuppressWarnings("deprecation")
    private static String hash(String toHash) {
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putString(toHash, StandardCharsets.UTF_8);
        return hasher.hash().toString();
    }
}
