package ru.mail.mantrov;


import java.util.ArrayList;
import java.util.List;


public class MainTask3 {
    public static final String hashCode = "ddc4035ff6451f85bb796879e9e5ea49";
    public static final int MAX_PASSWORD_LENGTH = 7;
    public final static List<Character> symbols = generateSymbols();
    public static String answer = null;

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        int threadsNumber = Runtime.getRuntime().availableProcessors();
        for (int i=0; i<threadsNumber; i++) {
            new Thread(new PasswordChecker(threadsNumber, i)).start();
        }

        while (answer==null) {
            Thread.sleep(100);
        }
        long endTime = System.currentTimeMillis();
        long time = endTime-startTime;
        String s = String.format("%02d:%02d:%02d", time / 1000 / 3600, time / 1000 / 60 % 60, time / 1000 % 60);
        System.out.println("Правильный пароль: " + answer);
        System.out.println("Потрачено " + s + ".");
    }


    public static List generateSymbols() {
        List symbols = new ArrayList();
        for (char c='a'; c<='z'; c++) {
            symbols.add(c);
        }
        return symbols;
    }

}
