package ru.mail.mantrov;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.xml.sax.helpers.AttributeListImpl;

import javax.management.DefaultLoaderRepository;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.logging.ConsoleHandler;

public class MainReflection {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        List<Object> list = new ArrayList<>();
        fillTheList(list);
        list = deprecatedFounder(list);
    }

    @SuppressWarnings("deprecation")
    private static void fillTheList(List<Object> list) {
        list.add(new AttributeListImpl());
        list.add(new DefaultLoaderRepository());
        list.add(new ConsoleHandler());
        list.add(4);
        list.add("string");
        list.add(new RMISecurityManager());
        list.add(new DeprecatedClass());
        list.add(new SecondDeprecatedClass());
    }

    private static List<Object> deprecatedFounder(List<Object> list) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException, ClassNotFoundException {
        List<Object> resultList = list.getClass().getConstructor().newInstance();
        for (Object obj : list) {
            Class<?> objClass = obj.getClass();
            Annotation[] annotations = objClass.getAnnotations();
            for (Annotation a : annotations) {
                if (a.annotationType().equals(java.lang.Deprecated.class)) {
                    resultList.add(obj);
                    findTheReplacement(objClass);
                }
            }
        }
        return resultList;
    }


    private static void findTheReplacement(Class<?> objClass) {
        List<Class<?>> classes = new ArrayList<>();
        String sDir = objClass.getPackageName();
        File dir = new File(sDir);
        Set<Class<?>> fileList = findInTheDirectory(dir);
        for (Class<?> checkingClass : fileList) {
            if(isSimilar(objClass, checkingClass)) {
                classes.add(checkingClass);
            }
        }
        printPossibleReplacement(objClass, classes);
    }

    private static Set<Class<?>> findInTheDirectory(File dir) {
        Reflections reflections = new Reflections(dir.getName(), new SubTypesScanner(false));
        Set<Class<? extends Object>> allClasses = reflections.getSubTypesOf(Object.class);
        return allClasses;
    }

    private static boolean isSimilar(Class<?> firstClass, Class<?> secondClass) {
        for (Annotation a : secondClass.getAnnotations()) {
            if (a.annotationType().equals(java.lang.Deprecated.class)) {
                return false;
            }
        }
        Class<?> superClass1 = firstClass.getSuperclass();
        if (superClass1.equals(secondClass)) {
            return true;
        }
        Class<?> superClass2 = secondClass.getSuperclass();
        if (!superClass1.equals(Object.class) && superClass1.equals(superClass2)) {
            return true;
        }
        Class<?>[] interfaces1 = firstClass.getInterfaces();
        Class<?>[] interfaces2 = secondClass.getInterfaces();
        for (Class<?> checkedI : interfaces1) {
            for (Class<?> i : interfaces2) {
                if (checkedI.equals(i)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void printPossibleReplacement(Class<?> deprecatedClass, Collection<Class<?>> replacementClasses) {
        String report = "Sorry, '" + deprecatedClass.getName() + "' is deprecated!\n";
        if (replacementClasses.isEmpty()) {
            System.out.println(report);
            return;
        }
        report += "But you can try ";
        for (Class<?> c : replacementClasses) {
            report += "'" + c.getName() + "' ";
        }
        report += "instead of deprecated one.\n";
        System.out.println(report);
    }

}
