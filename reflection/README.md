# Java Reflection

## Retrieving Class Objects

1. Look at the `Main` class and compare different methods of Objects Class retrieval.

## Retrieving Class Members

1. Implement the following application - you have an array of different objects, 
write a method which would iterate over array, find objects which classes annotated with 
`@Deprecated` and build a result array with objects found.
1. Pimp your method - add an output while iterating over the array. 
If object is instantiated from deprecated class, find an interface or parent class for it.
If such an interface or parent class exists, find non deprecated implementations or subclasses.
Print this classes as suggestions for usage instead of deprecated class. 

## Comments
Я попробовал написать что-то похожее: `ru.mail.mantrov.MainReflection`. При поиске классов с аннотацией `@Deprecated` 
проблем не возникло. С тем, чтобы спросить у класса от кого он унаследован и какие интерфейсы имплементит - тоже.
Проблемы начались, когда нужно было найти "implementations or subclasses". Кроме как искать перебором всех классов в
пакете - я ничего не придумал, но даже это сделать получилось не сразу. Стандартной библиотекой мне это удалось только с 
костылем, касающимся наименования деректории через `getAbsolutePath` директории. Этот костыль я заменил найденной библиотекой
`org.reflections`. И даже так - искать в пакетах стандартной java библиотеки у меня не получилось. Зато получилось
в своем пакете с подопытными классами и интерфейсами. В итоге я этот вариант и реализовал.